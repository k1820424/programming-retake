package me.tiago.programmingretake;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Position {
    private static int idNumber = 0;
    private String id;
    private String jobTitle;
    private String lineManagerId;
    private Position lineManager;
    private Map<String, Position> reportees;

    /**
     * Constructs the Position class
     * 
     * @param jobTitle
     */
    Position(String jobTitle) {
        this.reportees = new HashMap<>();
        this.jobTitle = jobTitle;
        idNumber += 1; // Increases static position count
        this.id = String.format("%s", idNumber);
    };

    /**
     * Retrieves the Line Manager ID
     * 
     * @return lineManagerId
     */
    public String getLineManagerId() {
        return lineManagerId;
    }

    /**
     * Retrieves the ID
     * 
     * @return ID
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the ID
     * 
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Retrieves the Job Title
     * 
     * @return jobTitle
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the Job Title
     * 
     * @param jobTitle
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Retrieves the line manager
     * 
     * @return lineManager
     */
    public Position getLineManager() {
        return lineManager;
    }

    /**
     * Sets the Line Manager and adds this position onto its reportees
     * 
     * @param lineManager
     */
    public void setLineManager(Position lineManager) {
        this.lineManager = lineManager;
        this.lineManagerId = lineManager.getId();

        lineManager.addReportee(this);
    }

    /**
     * 
     * @return Map of Reportees
     */
    public Map<String, Position> getReportees() {
        return reportees;
    }

    /**
     * Sets the reportees
     * 
     * @param reportees
     */
    public void setReportees(Map<String, Position> reportees) {
        this.reportees = reportees;
    }

    /**
     * Adds a reportee
     * 
     * @param reportee
     */
    public void addReportee(Position reportee) {
        this.reportees.put(reportee.getId(), reportee);
    }

    /**
     * Prints all reportees with their ID and Job Title
     */
    public void printReportees() {
        System.out.printf("%s's reportees:\nID | Job Title\n", this.getJobTitle());
        // Loop through member map entries
        for (Entry<String, Position> memberEntry : this.getReportees().entrySet()) {
            // Print entry with ID and Job Title
            System.out.printf("%s : %s\n", memberEntry.getKey(), memberEntry.getValue().getJobTitle());
        }

        System.out.println();
    }

}