package me.tiago.programmingretake;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Club {
    private String name;
    private Map<String, Position> members;
    private Position chair;

    /**
     * Constructor for the Club class
     * 
     * @param name
     */
    public Club(String name) {
        this.name = name;
        this.members = new HashMap<>();
    }

    /**
     * Retrieves the club name
     * 
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the club name
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Retrieves the club chairman
     * 
     * @return chair
     */
    public Position getChair() {
        return chair;
    }

    /**
     * Defines the club chairman
     * 
     * @param chair
     */
    public void setChair(Position chair) {
        this.chair = chair;
    }

    /**
     * Retrieves the member map
     * 
     * @return members
     */
    public Map<String, Position> getMembers() {
        return members;
    }

    /**
     * Set a member map to the club
     * 
     * @param members
     */
    public void setMembers(Map<String, Position> members) {
        this.members = members;
    }

    /**
     * Add a member to the club
     * 
     * @param member
     */
    public void addMember(Position member) {
        String positionTitle = member.getJobTitle();
        this.members.put(positionTitle, member);
    }

    public void addMembersFromFile(File csvFile) throws IOException {
        // Create buffer reader
        BufferedReader csvBufferedReader = new BufferedReader(new FileReader(csvFile));

        // Note:
        // CSV Layout:
        // Position Job Title | Line Manager

        // Loop through each line of the file and create every position
        String line;
        while ((line = csvBufferedReader.readLine()) != null) {
            // Create a Position and add it to the club
            String jobTitle = line.split(",")[0]; // Get Job Title
            Position clubMember = new Position(jobTitle);
            this.addMember(clubMember);

            if (line.contains("Chairman")) {
                // Set chairman
                this.setChair(clubMember);
            }
        }
        csvBufferedReader.close();

        csvBufferedReader = new BufferedReader(new FileReader(csvFile));
        // Loop through each line and set line managers
        while ((line = csvBufferedReader.readLine()) != null) {
            String jobTitle = line.split(",")[0]; // Get Job Title
            if (line.split(",").length > 1) {
                String lineManagerJobTitle = line.split(",")[1]; // Get Line Manager
                // Declare variables to be assigned later
                Position clubMember = null;
                Position lineManager;
                // Loop through lists to find correct position
                for (Position pos : this.findPositionByName(jobTitle)) {
                    // Find position by matching the job title name
                    if (pos.getJobTitle().matches(jobTitle)) {
                        clubMember = pos;
                    }
                }
                for (Position pos : this.findPositionByName(lineManagerJobTitle)) {
                    // Find line manager position by matching the job title name
                    if (pos.getJobTitle().matches(lineManagerJobTitle)) {
                        lineManager = pos;
                        // Set line manager
                        clubMember.setLineManager(lineManager);
                    }
                }

            }
        }

        // Close buffer reader
        csvBufferedReader.close();

    }

    /**
     * Removes a member from the club members
     * 
     * @param member
     */
    public void removeMember(Position member) {
        this.members.remove(member.getJobTitle());
    }

    /**
     * Prints all members
     */
    public void printMembers() {
        System.out.printf("%s's members: \n", this.getName());

        // Loop through member map entries
        for (Entry<String, Position> memberEntry : this.getMembers().entrySet()) {
            // Print entry
            System.out.printf("%s\n", memberEntry.getKey());
        }

        System.out.println();
    }

    /**
     * Finds every position that matches the searched keyword
     * 
     * @param PositionName
     * @return List of Positions
     */
    public List<Position> findPositionByName(String PositionName) {
        // Create a Position list
        List<Position> foundPositions = new ArrayList<>();
        // Loop through member map entries
        for (Entry<String, Position> memberEntry : this.getMembers().entrySet()) {
            // Check if the entry key (Position Name) contains the search keywords
            if (memberEntry.getKey().contains(PositionName)) {
                // Add found entry to list
                foundPositions.add(memberEntry.getValue());
            }
        }
        // Return all found positions in a list
        return foundPositions;
    }

}