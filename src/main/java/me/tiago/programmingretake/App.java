package me.tiago.programmingretake;


import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        AppGUI gui = new AppGUI();
        ClubData.club = new Club("Chelsea FC"); // An instance of club

        try {
            // Load positions from text file
            ClubData.club.addMembersFromFile(new File("inputfiles/clublinemanager.txt"));
            gui.setDefaultList(ClubData.club);
        } catch (IOException e) {
            System.out.printf("[WARNING]: %s", e.getMessage());
        }

        // club.printMembers();
        // club.findPositionByName("Chairman of the Board").get(0).printReportees();
        // club.findPositionByName("Executive").get(0).printReportees();
        // club.findPositionByName("Director of Football").get(0).printReportees();
        // club.findPositionByName("Manager").get(1).printReportees();
    }

}

class ClubData {

    public static Club club;

}

class AppGUI {

    public static DefaultListModel<String> positionList = new DefaultListModel<>();

    static JFrame frame; // Main window frame

    static JList<String> posList; // Position List
    static JScrollPane posListScrollPane; // List Scroll Pane
    static JPanel buttonPanel; // Panel that contains the buttons
    static JButton addButton, editButton, delButton; // Buttons

    static JDialog popupDialog; // Popup dialog for text input

    AppGUI() {

        // Name app window
        frame = new JFrame("Tiago's App");
        // Set list from DefaultListModel
        posList = new JList<>(positionList);
        // Set list settings
        posList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        posList.setSelectedIndex(0);
        posList.setVisibleRowCount(5);
        // Add scroll pane to list
        posListScrollPane = new JScrollPane(posList);
        // Button panel
        buttonPanel = new JPanel();
        // Buttons
        addButton = new JButton("Add");
        editButton = new JButton("Edit");
        delButton = new JButton("Delete");
        // Adding buttons to button panel
        buttonPanel.add(addButton);
        buttonPanel.add(editButton);
        buttonPanel.add(delButton);
        // Link buttons to actions
        addButton.addActionListener(GUIActionListener.listActionListener(posList));
        editButton.addActionListener(GUIActionListener.listActionListener(posList));
        delButton.addActionListener(GUIActionListener.listActionListener(posList));
        // Frame settings
        frame.setSize(480, 480);
        frame.setVisible(true);
        // Add everything to frame
        frame.add(new JPanel(), BorderLayout.NORTH);
        frame.add(new JPanel(), BorderLayout.EAST);
        frame.add(new JPanel(), BorderLayout.WEST);
        frame.add(posListScrollPane, BorderLayout.CENTER);
        frame.add(buttonPanel, BorderLayout.SOUTH);
    }

    void setDefaultList(Club club) {
        // Load positions onto default GUI list
        for (Entry<String, Position> position : club.getMembers().entrySet()) {
            AppGUI.positionList.addElement(position.getKey());
        }
    }

    public static void refreshFrame() {
        frame.invalidate();
        frame.validate();
        frame.repaint();
    }

}

class GUIActionListener {
    /**
     * Action listener that takes in an JList as a parameter. This method is used on
     * action listeners that require the current list state
     * 
     * @param jList
     * @return
     */
    // public final static Club CLUB_OBJECT = ;

    static ActionListener listActionListener(JList<?> jList) {

        final JList<?> list = jList; // Necessary for later referenc
        // Create and return Action Listener object
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Assign appropriate action based on action command
                switch (e.getActionCommand()) {
                    case "Add":
                        onAddButtonPress(list);
                        break;
                    case "Edit":
                        if (list.getSelectedIndex() != -1) {
                            onEditButtonPress(list);
                        } else {
                            showMessageDialog(null, "You need to have an item selected", "Error deleting entry",
                                    ERROR_MESSAGE);
                            break;
                        }
                        break;
                    case "Delete":
                        if (list.getSelectedIndex() != -1) {
                            onDeleteButtonPress(list);
                        } else {
                            showMessageDialog(null, "You need to have an item selected", "Error deleting entry",
                                    ERROR_MESSAGE);
                            break;
                        }
                        break;
                    default:
                        break;
                }

            }
        };
    }

    /**
     * Method to be called by the "Add" button. Adds a new entry
     * 
     * @param list
     */
    private static void onAddButtonPress(JList<?> list) {
        // Get user input from input dialog
        String newEntryName = (String) showInputDialog(null, "Input new entry name:", "Add entry", PLAIN_MESSAGE, null,
                null, "");
        // Add entry name if a string was returned
        if ((newEntryName != null) && (newEntryName.length() > 0)) {
            // Store old values
            Map<String, Position> clubMemberMap = ClubData.club.getMembers();
            // Create new position with new entry name
            Position pos = new Position(newEntryName);
            // Replace member map and set it
            clubMemberMap.put(newEntryName, pos);
            ClubData.club.setMembers(clubMemberMap);
            // GUI
            AppGUI.positionList.addElement(newEntryName); // Add item in GUI list model
            AppGUI.refreshFrame(); // Refresh GUI frame
        }
    }

    /**
     * Method to be called by the "Edit" button. Edits a selected entry
     * 
     * @param list
     */
    private static void onEditButtonPress(JList<?> list) {
        String editEntry = list.getSelectedValue().toString();
        // Find exact member in club data to edit
        for (Position pos : ClubData.club.findPositionByName(editEntry)) {
            // Find exact position matching entry name
            if (pos.getJobTitle().matches(editEntry)) {
                // Get user input from input dialog
                String newEntryName = (String) showInputDialog(null, "Input new name:", "Edit entry", PLAIN_MESSAGE,
                        null, null, editEntry);
                // Change entry name if a string was returned
                if ((newEntryName != null) && (newEntryName.length() > 0)) {
                    // Store old values
                    String oldMapKey = pos.getJobTitle();
                    Map<String, Position> clubMemberMap = ClubData.club.getMembers();
                    // Replace job title with new input
                    pos.setJobTitle(newEntryName);
                    // Replace member map and set it
                    clubMemberMap.remove(oldMapKey);
                    clubMemberMap.put(newEntryName, pos);
                    ClubData.club.setMembers(clubMemberMap);
                    // GUI
                    AppGUI.positionList.set(list.getSelectedIndex(), newEntryName); // Edit item in GUI list model
                    AppGUI.refreshFrame(); // Refresh GUI frame
                }
            }
        }
    }

    /**
     * Method to be called by the "Delete" button. Deletes a selected entry
     * 
     * @param
     */
    private static void onDeleteButtonPress(JList<?> list) {
        String deleteEntry = list.getSelectedValue().toString();
        // Deletes club member from club data
        for (Position pos : ClubData.club.findPositionByName(deleteEntry)) {
            // Find exact position matching entry name
            if (pos.getJobTitle().matches(deleteEntry)) {
                ClubData.club.removeMember(pos);
                AppGUI.positionList.remove(list.getSelectedIndex()); // Remove item from GUI list model
                AppGUI.refreshFrame(); // Refresh GUI frame
            }
        }
    }

}
